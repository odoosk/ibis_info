# -*- encoding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
import re
from odoo.exceptions import ValidationError
import logging as log


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    @api.onchange('product_id')
    def get_additional_name(self):
        self.additional_name = self.product_id.additional_name


    additional_name = fields.Char('تسمية عربية',default=lambda self:self.product_id.additional_name)




class StockPicking(models.Model):
    _inherit = 'stock.picking'


    def button_validate(self):
        res = super(StockPicking, self).button_validate()
        for move in self.move_ids_without_package:
            move.product_id.additional_name = move.additional_name

        return res