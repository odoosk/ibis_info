# -*- encoding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
import re
from odoo.exceptions import ValidationError
import logging as log



class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    @api.multi
    def _prepare_stock_moves(self,picking):
        res = super(PurchaseOrderLine, self)._prepare_stock_moves(picking)
        res[0]['additional_name'] = self.product_id.additional_name 
        #raise ValidationError(res[0]['product_id'])
        return res