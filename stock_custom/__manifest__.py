# -*- coding: utf-8 -*-
##########################################################################
###################################################################################
{
    'name': 'CUSTOM STOCK',
    'version': '13.0.1.1.2',
    'summary': """ajouter le nom arabe dans le bon de réception""",
    'description': """  """,
    'category': 'accounting',
    'author': 'SEKKAK OUSSAMA',
    'company': 'IBIS INFORMATIQUE',
    'website': "",
    'depends': ['base','sale'],
    'data': [
        'views/stock.xml',

    ],
    'icon_image': ['static/description/icon.png'],
    'images': ['static/description/access_group.png','static/description/session.png','static/description/session_trainer.png'],
    'license': 'AGPL-3',
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
