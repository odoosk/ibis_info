# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
#
# Copyright (c) 2019

from . import timbre
from . import payment_term
from . import account_invoice
from . import purchase_order
from . import sale_order
