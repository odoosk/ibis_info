# -*- encoding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
import re
from odoo.exceptions import ValidationError
import logging as log



class BudgetBudget(models.Model):
	_name = 'budget.budget'

	name = fields.Char('Référence',default='Nouveau',readonly=True)
	user_id = fields.Many2one('res.users','Résponsable')
	amount_budget = fields.Float('Montant total')
	date_from = fields.Date('Date debut')
	date_to = fields.Date('Date fin')
	project_id = fields.Many2one('project.project','Projet')
	line_ids = fields.One2many('budget.line','budget_id','Lignes')
	state = fields.Selection([('draft','Brouillon'),('done','Validé')],default='draft')
	percentage = fields.Float('Approvisionnement %',compute="get_percentage")
	percentage_out = fields.Float('Consommation %',compute="get_percentage_out")


	@api.one
	def get_percentage(self):
		if self.line_ids:
			self.percentage = sum(l.percentage for l in self.line_ids)/float(len(self.line_ids))
		return self.percentage

	@api.one
	def get_percentage_out(self):
		if self.line_ids:
			self.percentage_out = sum(l.percentage_out for l in self.line_ids)/float(len(self.line_ids))
		return self.percentage_out

	@api.one
	def do_done(self):
		
		self.name = 'B'+'/'+str(self.project_id.code)+'/'+self.env['ir.sequence'].next_by_code('reg.appro') or 'Nouveau'
		self.state = 'done'

	@api.one
	def do_draft(self):
		self.state = 'draft'

class BudgetBudget(models.Model):
	_name = 'budget.line'

	budget_id = fields.Many2one('budget.budget','Budget')
	product_id = fields.Many2one('product.product','Article')
	date_from = fields.Date('Date debut',related='budget_id.date_from')
	date_to = fields.Date('Date fin',related='budget_id.date_to')
	qty = fields.Float('Qté à approvisionner')
	real_qty = fields.Float('Qté approvisionnée',compute='get_qty')
	real_qty_out = fields.Float('Qté Consommée',compute='get_qty_out')
	price = fields.Float('Montant')
	project_id = fields.Many2one('project.project','Projet',related='budget_id.project_id',store=True)
	percentage = fields.Float('Réception %',compute='get_percentage')
	percentage_out = fields.Float('Consommation %',compute='get_percentage_out')
	#block = fields.Boolean('Bloquer achat')


	@api.one
	def get_qty(self):
		quantite = 0.0
		warehouse_id = self.env['stock.warehouse'].search([('id','=',self.project_id.warehouse_id.id)],limit=1)[0]
		picking_type_id = self.env['stock.picking.type'].search([('warehouse_id','=',warehouse_id.id),('code','=','incoming')])[0]
		receptions = self.env['stock.move.line'].search([('date','>=',self.date_from),
													('date','<=',self.date_to),
													('product_id','=',self.product_id.id),
													('state','=','done'),
													('location_dest_id','=',warehouse_id.lot_stock_id.id),
													('location_id','!=',warehouse_id.lot_stock_id.id)])
		#raise ValidationError(str(receptions))
		for record in receptions:
			quantite += record.qty_done

		self.real_qty = quantite

		return quantite

	@api.one
	def get_qty_out(self):
		quantite = 0.0
		warehouse_id = self.env['stock.warehouse'].search([('id','=',self.project_id.warehouse_id.id)],limit=1)[0]
		pickout = self.env['stock.move.line'].search([('date','>=',self.date_from),
													('date','<=',self.date_to),
													('product_id','=',self.product_id.id),
													('state','=','done'),
													('location_id','=',warehouse_id.lot_stock_id.id),
													('location_dest_id','!=',warehouse_id.lot_stock_id.id)])
		#raise ValidationError(str(receptions))
		for record in pickout:
			quantite += record.qty_done

		self.real_qty_out = quantite

		return quantite

	@api.one
	def get_percentage(self):
		self.percentage = ( self.real_qty / self.qty ) * 100
		return self.percentage

	@api.one
	def get_percentage_out(self):
		self.percentage_out = ( self.real_qty_out / self.qty ) * 100
		return self.percentage