# -*- coding: utf-8 -*-
##########################################################################
###################################################################################
{
    'name': 'Gestion de budget',
    'version': '12.0.1.1.2',
    'summary': """Ajouter possibilité de controler les demandes achats a partir d'un budget bien défini dans le projet""",
    'description': """  """,
    'category': 'project',
    'author': 'SEKKAK OUSSAMA',
    'company': 'IBIS INFORMATIQUE',
    'website': "",
    'depends': ['base','project','demande_appro'],
    'data': [
        'views/budget_view.xml',
        'data/ir_sequence.xml'

    ],
    'icon_image': ['static/description/icon.png'],
    'images': ['static/description/access_group.png','static/description/session.png','static/description/session_trainer.png'],
    'license': 'AGPL-3',
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
