# Copyright 2017 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Appro Tier Validation",
    "summary": "Extends the functionality of Procurment to "
               "support a tier validation process.",
    "version": "12.0.1.0.0",
    "category": "Procurment",
    "author": "OSK OUSSAMA SEKKAK, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "demande_appro",
        "base_tier_validation",
    ],
    "data": [
        "views/da_procurment_view.xml",
    ],
}
