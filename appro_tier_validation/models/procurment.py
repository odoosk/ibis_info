# Copyright 2017 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import models


class DaProcurment(models.Model):
	_name = "da.procurment"
	_inherit = ['da.procurment', 'tier.validation']
	_state_from = ['sent']
	_state_to = ['progress']


	def _validate_tier(self, tiers=False):
		res = super(DaProcurment, self)._validate_tier(tiers)
		self.state = 'progress'
		return res