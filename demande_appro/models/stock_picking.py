# -*- coding: utf-8 -*-
###################################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: cybrosys(<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
import pytz
import sys
import datetime
import logging
import binascii
from struct import unpack
from odoo import api, fields, models
from odoo import _
from odoo.exceptions import UserError, ValidationError 

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
	_inherit = 'stock.picking'


	type_pick = fields.Selection([('bsm','BSM'),('br','Reception')])


	@api.multi
	def print_report(self):
		if self.type_pick == 'bsm':
			return self.env.ref('demande_appro.action_report_bsm').report_action(self)
		elif self.type_pick == 'br':
			return self.env.ref('demande_appro.action_report_be').report_action(self)
		else:
			return False

	@api.multi
	def open_demande_commande(self):
		self.ensure_one()
		purchase = self.env['purchase.order'].search([('name', 'ilike', self.origin)])
		action = self.env['ir.actions.act_window'].for_xml_id('purchase',"purchase_rfq")
		action["context"] = {"create": False}
		if len(purchase) > 1:
			action['domain'] = [('id', 'in', purchase.ids)]
		elif len(purchase) == 1:
			form_view = [(self.env.ref('purchase.purchase_order_form').id, 'form')]
			if 'views' in action:
				action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
			else:
				action['views'] = form_view
			action['res_id'] = purchase.ids[0]
		else:
			action = {'type': 'ir.actions.act_window_close'}
		return action


class ResUsers(models.Model):
	_inherit = 'res.users'

	warehouse_ids = fields.Many2many('stock.warehouse',string="Entrepos autorisés")