# -*- coding: utf-8 -*-
###################################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: cybrosys(<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
import pytz
import sys
import datetime
import logging
import binascii
from struct import unpack
from odoo import api, fields, models
from odoo import _
from odoo.exceptions import UserError, ValidationError 

_logger = logging.getLogger(__name__)


class ProjectProject(models.Model):
	_inherit = 'project.project'

	code = fields.Char('Code Projet',required=True)
	address = fields.Char('Adresse')
	state_id = fields.Many2one('res.country.state','Wilaya')
	employee_ids = fields.Many2many('hr.employee')
	warehouse_id = fields.Many2one('stock.warehouse','Entrepot')
	title = fields.Text('Description')
	# filtered_user_ids= fields.Many2many('res.users',compute='get_users')


	@api.onchange('partner_id')
	def onchange_address(self):
		for record in self:
			record.address = record.partner_id.street

	# @api.multi
	# def get_users(self):
	# 	for record in self:
	# 		for employee in record.employee_ids:
	# 			#res.append(employee.user_id.id)
	# 			record.filtered_user_ids = (4,employee.user_id.id)
	# 		return record.filtered_user_ids




	@api.multi
	def open_demande_approvisionnement(self):
		self.ensure_one()
		appros = self.env['da.procurment'].search([('project_id', '=', self.id)])
		action = self.env['ir.actions.act_window'].for_xml_id('demande_appro',"open_approvisionnement_tree")
		action["context"] = {"create": False}
		if len(appros) > 1:
			action['domain'] = [('id', 'in', appros.ids)]
		elif len(appros) == 1:
			form_view = [(self.env.ref('demande_appro.view_approvisionnement_form').id, 'form')]
			if 'views' in action:
				action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
			else:
				action['views'] = form_view
			action['res_id'] = appros.ids[0]
		else:
			action = {'type': 'ir.actions.act_window_close'}
		return action

