# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import datetime
from odoo import models, fields, api, SUPERUSER_ID,exceptions
from datetime import datetime, timedelta, date
import time
from dateutil.relativedelta import relativedelta
from odoo.osv import osv
from odoo.tools.translate import _
import math
from odoo.exceptions import ValidationError, Warning


class PurchaseRequest(models.Model):
	_name = 'purchase.request'
	_order = 'date desc'
	_desecription = "Demande d'achat"


	name = fields.Char(string="Référence",readonly=True,default="/")
	description =fields.Text(string='Description')
	state = fields.Selection([('draft', 'Brouillon'),('sent','Envoyée'),('devis','Demande Prix Crée'),('done','BC Crée'),('canceled','Annulée')],string="Etat",default="draft")
	date = fields.Datetime('Date')
	validator = fields.Many2one('hr.employee','Validée par')
	applicant = fields.Many2one('hr.employee','Demandeur',required=True)
	request_lines = fields.One2many("purchase.request.line","request_id",string="Articles")
	origin = fields.Char("Document d'origine")
	purchase_ids = fields.Many2many("purchase.order",string="Achats")
	company_id = fields.Many2one('res.company',string='Company', default=lambda self: self.env.user.company_id )
	project_id = fields.Many2one('project.project','Projet')
	purchase_count = fields.Integer('Nombre des achats',compute='count_purchase')


	@api.multi
	def count_purchase(self):
		if self.purchase_ids:
			self.purchase_count = len(self.purchase_ids)
		else:
			return 0


	@api.one
	def confirm_purchase_request(self):
		self.state="sent"
		self.name = 'DA/'+str(self.project_id.code)+'/'+self.env[('ir.sequence')].get('dem.achat')



	def open_wizard_purchase(self):
		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'procurement.purchase',
			'target': 'new',
			'type': 'ir.actions.act_window',
			'context': {'default_request_id': self.id}
		} 
	


	@api.multi
	def open_demande_commande(self):
		self.ensure_one()
		purchase = self.env['purchase.order'].search([('id', 'in', self.purchase_ids.ids)])
		action = self.env['ir.actions.act_window'].for_xml_id('purchase',"purchase_rfq")
		action["context"] = {"create": False}
		if len(purchase) > 1:
			action['domain'] = [('id', 'in', purchase.ids)]
		elif len(purchase) == 1:
			form_view = [(self.env.ref('purchase.purchase_order_form').id, 'form')]
			if 'views' in action:
				action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
			else:
				action['views'] = form_view
			action['res_id'] = purchase.ids[0]
		else:
			action = {'type': 'ir.actions.act_window_close'}
		return action

class PurchaseRequestLine(models.Model):
	_name = 'purchase.request.line'

	@api.one
	@api.depends('product_id')
	def get_product_uom(self):
		self.uom_id = self.product_id.uom_id.id
		return self.uom_id.id			

	product_id = fields.Many2one('product.product',string='Article')
	additional_name = fields.Char('تسمية عربية',related='product_id.additional_name')
	uom_id = fields.Many2one('uom.uom',compute='get_product_uom',string='Udm')
	qty = fields.Float(string="Quantité",default=1.0)
	name = fields.Char(string="Descritption")#product_name
	request_id = fields.Many2one('purchase.request',string="Demande d'achat")


class ProcurementToPurchase(models.TransientModel):
	_name = 'procurement.purchase'

	@api.model
	def search_employee(self,user_id):
		for record in self:
			employee = self.env['hr.employee'].search([('user_id', '=', user_id)],limit=1)
			if employee:
				return employee.id
			else:
				raise ValidationError("Prière d'indiquer l'utilisateur de l'employé %s", user_id.name)

	partner_id = fields.Many2one('res.partner','Fournisseur',domain=[('supplier','=',True)])
	date = fields.Datetime('Date',default=lambda self:fields.datetime.now())

	def default_currency_id(self):
		company_id = self.env.context.get('force_company') or self.env.context.get('company_id') or self.env.user.company_id.id
		return self.env['res.company'].browse(company_id).currency_id.id

	@api.multi
	def generate_purchase_order(self):
		res = False
		vals = {}
		values = {}

		purchase_request = self.env['purchase.request'].browse(self._context.get('active_ids', []))
		for record in self:
			for rec in purchase_request:
				picking_type_id = self.env['stock.picking.type'].search([('warehouse_id','=',rec.project_id.warehouse_id.id),('code','=','incoming')])[0]
				vals.update({
					'partner_id': record.partner_id.id,
					'pricelist_id':2,
					'currency_id':record.default_currency_id(),
					'invoice_method':'manual',
					'location_id':12,
					'picking_type_id':picking_type_id.id,
					'date_order': self.date,
					'project_id':rec.project_id.id,
				})
				
				res = rec.env[('purchase.order')].create(vals)
				break
			name = ''
			for rec in purchase_request:
				name += rec.name + '/'
				res.write({
				'origin':name,
				'request_ids':[(4, rec.id)],
				})
				for item in rec.request_lines:
					values.update({
						'partner_id':self.partner_id.id,
						'product_id':item.product_id.id,
						'price_unit':0.0,
						'product_uom':item.uom_id.id,
						'date_planned':rec.date,
						'name':item.product_id.name,
						'product_qty':item.qty,
						'order_id':res.id,
						'currency_id': self.default_currency_id()
						

						})
					result=self.env[('purchase.order.line')].create(values)
				rec.state = 'devis'
				rec.validator = self.search_employee(self.env.user.id)
		return True

