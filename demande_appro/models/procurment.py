# -*- coding: utf-8 -*-
###################################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: cybrosys(<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
import pytz
import sys
import datetime
import logging
import binascii
from struct import unpack
from odoo import api, fields, models
from odoo import _
from odoo.exceptions import UserError, ValidationError 

_logger = logging.getLogger(__name__)


class DaProcurment(models.Model):
	_name = 'da.procurment'
	_order = 'date desc'
	_inherit = ['mail.thread']
	_description = "demande d'approvisionnement"

	@api.model
	def _default_employee(self):
		employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)],limit=1)
		return employee.id if employee else False




	@api.multi
	def open_purchase_request(self):
		self.ensure_one()
		requests = self.env['purchase.request'].search([('origin', '=', self.name)])
		action = self.env['ir.actions.act_window'].for_xml_id('demande_appro',"purchase_request_action")
		action["context"] = {"create": False}
		if len(requests) > 1:
			action['domain'] = [('id', 'in', requests.ids)]
		elif len(requests) == 1:
			form_view = [(self.env.ref('demande_appro.purchase_request_view_form').id, 'form')]
			if 'views' in action:
				action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
			else:
				action['views'] = form_view
			action['res_id'] = requests.ids[0]
		else:
			action = {'type': 'ir.actions.act_window_close'}
		return action


	@api.multi
	def open_stock_picking(self):
		self.ensure_one()
		picking = self.env['stock.picking'].search([('origin', '=', self.name)])
		action = self.env['ir.actions.act_window'].for_xml_id('stock',"action_picking_tree")
		action["context"] = {"create": False}
		if len(picking) > 1:
			action['domain'] = [('id', 'in', picking.ids)]
		elif len(picking) == 1:
			form_view = [(self.env.ref('stock.view_picking_form').id, 'form')]
			if 'views' in action:
				action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
			else:
				action['views'] = form_view
			action['res_id'] = picking.ids[0]
		else:
			action = {'type': 'ir.actions.act_window_close'}
		return action


	#declaration of fields
	name = fields.Char('N° Demande')
	type_demande = fields.Selection([('service','Préstation de service'),('appro','Approvisionnement'),('interne','Transfert Interne')],states={'draft': [('readonly', False)]},default='appro',translate=True)
	categ_id = fields.Selection([('material','Materiaux de constuction'),('pdr','Pièces de rechange')],default='pdr',string='Categorie')
	nature = fields.Selection([('consume','Consommation'),('invest','Investissement')],readonly=True,states={'draft': [('readonly', False)]},translate=True)
	#partner_id = fields.Many2one('res.partner','Fournisseur')	
	demandeur_id = fields.Many2one('hr.employee','Demandeur',track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]},required=True,default=_default_employee)
	validator_id = fields.Many2one('hr.employee','Valider par',trackable=True)
	state = fields.Selection([('draft','Brouillon'),('sent','Demande Envoyée'),('progress','En traitement'),('done','Terminée'),('cancled','Annulée')],trackable=True,default='draft')
	procurment_line = fields.One2many('da.procurment.line','procurment_id','Lignes de demande',readonly=True,states={'draft': [('readonly', False)]})
	date = fields.Datetime('Date demande',readonly=True, default=fields.Datetime.now,states={'draft': [('readonly', False)]})
	location_id = fields.Many2one('stock.warehouse','Entrepos source',readonly=True,states={'draft': [('readonly', False)]},track_visibility='onchange')
	location_dest_id = fields.Many2one('stock.warehouse','Entrepos destination',readonly=True,states={'draft': [('readonly', False)]},track_visibility='onchange')
	note = fields.Text('Autres description')
	company_id = fields.Many2one('res.company', string='Company', required=True,
		default=lambda self: self.env['res.company']._company_default_get('account.account'))
	project_id = fields.Many2one('project.project',string="Nom du projet",track_visibility='onchange',readonly=True,states={'draft': [('readonly', False)]})
	#code = fields.Char(string="Code du projet")
	#adresse = fields.Char(string="Adresse du projet")
	#chef_projet = fields.Many2one('hr.employee',string="Nom du chef de projet")
	#service = fields.Selection([('achat','Achat & Approvisionnement '),('marche','Marché'),('rh','Ressources Humaines'),('operations','Opérations Diverses'),('reparation','Réparation')],states={'draft': [('readonly', False)]})
	approuved=fields.Boolean('Approuved ?')
	#request_id = fields.Many2one('purchase.request',string="Demande d'achat")
	#signature_magasin = fields.Binary('Signature Magasinier',help='Signature du magasinier',copy=False,attachment=True,max_with=1024,max_height=1024)
	#signature_achat = fields.Binary('Signature Acheteur',help='Signature responsable des achats', copy=False,attachment=True,max_with=1024,max_height=1024)
	#signature_demandeur = fields.Binary('Signature Demandeur',help='Signature responsable des achats', copy=False,attachment=True,max_with=1024,max_height=1024)
	picking = fields.Boolean('Mouvement de stock Crée')
	requested = fields.Boolean('Demande achat crée')
	#project_id = fields.Many2one('project.project',related='procurment_id.project_id',string='Projet')



	@api.onchange('demandeur_id')
	def _get_project_domain(self):
		self.ensure_one()
		projects = self.demandeur_id.project_ids.mapped('id') 
		if projects:
			self.project_id = projects[0]
			return  {
			'domain':{'project_id':[('id', 'in', projects)]}}
		else:
			return  {'domain':{'project_id':[('id', 'in', [])]}}



	@api.onchange('project_id')
	def _get_warehouse_domain(self):
		self.ensure_one()

		warehouse_id = self.project_id.warehouse_id.id 
		if warehouse_id:
			self.location_dest_id = warehouse_id
			return  {
			'domain':{'location_dest_id':[('id', '=', warehouse_id)]}}
		else:
			return  {'domain':{'location_dest_id':[('id', 'in', [])]}}


	@api.model
	def search_employee(self,user_id):
		for record in self:
			employee = self.env['hr.employee'].search([('user_id', '=', user_id)],limit=1)
			if employee:
				return employee.id
			else:
				raise ValidationError("Prière d'indiquer l'utilisateur de l'employé %s", user_id.name)

	@api.one
	def confirme_appro(self):
		res = self.sudo().write({'validator_id':self.search_employee(self.env.user.id),
											   #'signature_magasin':self.signature,
											   'state':'done'
										})
		return res


	@api.one
	def confirme_demandeur(self):
		res = self.sudo().write({
										 #'signature_demandeur':self.signature,
										 'state':'sent'
										})
		return res



	def do_sent(self):
		if len(self.procurment_line) > 0: 
			return self.confirme_demandeur()
		else:
			raise ValidationError("Prière d'indiquer les articles à approvisionner dans votre demande")



	def do_done(self):
		for rec in self:
			if all(o.operation != 'draft' for o in rec.procurment_line):
				return self.confirme_appro()
			else:
				raise UserError('Merci de traiter tous les lignes avant de cliquer sur Terminer !')

	def do_cancel(self):
		for rec in self:
			rec.sudo().write({'state':'cancled'})
			

	def do_draft(self):
		for rec in self:
			rec.sudo().write({'state':'draft'})


	@api.model
	def create(self,vals):
		#sequence=self.env[('ir.sequence')].get('reg_code_paie'
		code = self.env['project.project'].search([('id','=',vals['project_id'])]).code
		vals['name'] ='DAP'+'/'+str(code)+'/'+self.env['ir.sequence'].next_by_code('reg.appro') or 'Nouveau'
		res = super(DaProcurment, self).create(vals)
		
		return res


	@api.multi
	def open_demande_approvisionnement(self):
		self.ensure_one()
		appros = self.env['da.procurment'].search([('project_id', '=', self.id)])
		action = self.env['ir.actions.act_window'].for_xml_id('stock',"stock_picking_action_picking_type")
		action["context"] = {"create": False}
		if len(appros) > 1:
			action['domain'] = [('id', 'in', appros.ids)]
		elif len(appros) == 1:
			form_view = [(self.env.ref('stock.view_picking_form').id, 'form')]
			if 'views' in action:
				action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
			else:
				action['views'] = form_view
			action['res_id'] = appros.ids[0]
		else:
			action = {'type': 'ir.actions.act_window_close'}
		return action



class DaProcurmentLine(models.Model):
	_name = 'da.procurment.line'
	_order = 'name desc'
	_description = "lignes de D.A"



	name = fields.Char(compute='compute_name',index=True,default="Nouveau")
	date = fields.Date('Date prevue',default=fields.Date.today())
	procurment_id = fields.Many2one('da.procurment','Approvisionnement N°')
	location_id = fields.Many2one('stock.warehouse','Entrepos source',related='procurment_id.location_id')
	location_dest_id = fields.Many2one('stock.warehouse','Entrepos destination',related='procurment_id.location_dest_id')
	product_id = fields.Many2one('product.product','Article',index=True,required=True)
	uom_id = fields.Many2one('uom.uom',compute='get_product_uom')
	uom_qty = fields.Float('Quantité') 
	#is_requested = fields.Boolean("Déja demandé",default=False,readonly=True)
	operation = fields.Selection([('draft','A traiter'),('requested','A acheter'),('transferred','A transférer'),('consum','A consommer'),('autre','Autre')],default='draft')
	qty_available = fields.Float("Qty disponible" ,related="product_id.qty_available")
	note = fields.Char('Observation')
	additional_name = fields.Char('Nom supplémentaire',related='product_id.additional_name')

	qty_bsm = fields.Float('Qty a Consommer')
	qty_da = fields.Float('Qty a Acheter')
	qty_bt = fields.Float('Qty a Transférer')
	#request_id = fields.Many2one('purchase.request','Demande d\'achat',related='procurment_id.request_id')
	#project_id = fields.Many2one('project.project','Projet',related="procurment_id.project_id")


	@api.model
	def create(self,vals):
		res = False
		procurment_id = self.env['da.procurment'].search([('id','=',vals['procurment_id'])])
		project_id = self.env['project.project'].search([('id','=',procurment_id.project_id.id)],limit=1)
		check_qty_budget = self.env['budget.line'].search([('product_id','=',vals['product_id']),('date_from','<=',vals['date']),('date_to','>=',vals['date']),('project_id','=',project_id.id)],limit=1)
		if float(vals['uom_qty']) < 1.0:
			raise UserError("Attention , veuillez selectionner la quantité à approvisionner")
		else:
			res = super(DaProcurmentLine, self).create(vals)
		
		return res

	@api.multi
	def write(self,vals):
		res = False
		product_id = vals.get('product_id') or self.product_id.id
		date = vals.get('date') or self.date
		uom_qty = vals.get('uom_qty') or self.uom_qty
		project_id = self.env['project.project'].search([('id','=',self.procurment_id.project_id.id)],limit=1)
		check_qty_budget = self.env['budget.line'].search([('product_id','=',product_id),('date_from','<=',date),('date_to','>=',date),('project_id','=',project_id.id)],limit=1)
		if float(uom_qty) < 1.0:
			raise UserError("Attention , veuillez selectionner la quantité à approvisionner")
		else:
			res = super(DaProcurmentLine, self).write(vals)
		
		return res


	@api.one
	def do_action(self):
		res = {}
		# if self.qty_bsm + self.qty_da + self.qty_bt > self.uom_qty :
		# 	raise UserError("Attention , La somme des quantités à traiter dépasse la quantité à approvisionner")
		if self.qty_bsm > 0:
			res = self.create_picking('Consommation',float(self.qty_bsm))
		if self.qty_da > 0:
			res = self.create_purchase_request(float(self.qty_da))
		if self.qty_bt> 0:
			res = self.create_picking('Transferts',float(self.qty_bt))
		if res:
			self.operation = 'autre'
		else:
			raise UserError("Attention , Vous n'avez pas encore choisi une quantité à traiter")
		return res


	def open_wizard(self):
		#raise UserError(active_id)
		context = self.env.context
		return {
			'type': 'ir.actions.act_window',
			'name': 'Line Approvisionnement',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': self._name,
			'res_id': self.id,
			'target': 'new',
		}
	@api.depends('product_id')
	def get_product_uom(self):
		for rec in self:
			rec.uom_id = rec.product_id.uom_id.id
			return rec.uom_id.id

	@api.one
	@api.depends('product_id','procurment_id')
	def compute_name(self):
		for rec in self:
			if rec.procurment_id:
				name = rec.procurment_id.name +" ["+rec.product_id.name+"]"
			else:
				if self._context['active_id']:
					procurment_id = self.env['da.procurment'].browse(self._context['active_id'])
					name = procurment_id.name +" ["+rec.product_id.name+"]"
			rec.name = name


	@api.one
	def create_purchase_request(self,qty):
		vals = {}
		#raise UserError(qty)
		res= {}
		if float(qty) > 0.0 :
			if not self.procurment_id.validated:
				raise UserError("Attention , demande d'approvisionnement n'est pas encore approuvée")
			else:
				procurement_id = False
				# procurement_id = self.env['internal.procurement'].search([
				# 		("id","=",self.procurment_id.id)
				# 	])
				request_id = self.env['purchase.request'].search([
					("origin","=", self.procurment_id.name)
					])
				if request_id:
					res=self.env['purchase.request.line'].create({
							 "request_id":request_id.id,
							 'product_id':self.product_id.id,
							 'qty': qty,
							 'uom_id':self.uom_id.id,
							 'name':self.product_id.name,
							 
						})
				else:
					vals = {
						"applicant":self.procurment_id.demandeur_id.id,
						"date":self.procurment_id.create_date,
						'origin':self.procurment_id.name,
						'description':self.procurment_id.note,
						'project_id':self.procurment_id.project_id.id,
						"request_lines":[(0,0, {

														'product_id':self.product_id.id,
														'qty': qty,
														'uom_id':self.uom_id.id,
														'name':self.product_id.name,
														   
					})]
						}
					res=self.env['purchase.request'].create(vals)
				self.procurment_id.requested = True
			return res
		else:
			raise UserError("Attention , veuillez selectionner la quantité à acheter")
	@api.one
	def create_pr(self):

		product_id = self.product_id.id
		date = self.date
		uom_qty = self.uom_qty
		project_id = self.env['project.project'].search([('id','=',self.procurment_id.project_id.id)],limit=1)
		check_qty_budget = self.env['budget.line'].search([('product_id','=',product_id),('date_from','<=',date),('date_to','>=',date),('project_id','=',project_id.id)],limit=1)
		if check_qty_budget and check_qty_budget.budget_id.state == 'done' and (float(uom_qty) + float(check_qty_budget.real_qty)) > float(check_qty_budget.qty):
			raise UserError("Attention , Vous avez dépassé la quantité déclarée dans le budget"+'>'+str(check_qty_budget.qty))
						
		res = self.create_purchase_request(uom_qty)

		if res:
			self.operation ='requested'
			return res


	@api.one
	def create_picking(self,type_picking,qty):
		vals = {}
		res=False
		picking_type_id = self.env['stock.picking.type'].search([
																		('warehouse_id','=',self.location_id.id),
																		('name','ilike',type_picking)])
		if not self.procurment_id.validated:
			raise UserError("Attention , demande d'approvisionnement n'est pas encore approuvée")
		else:
			
			procurement_id = False
			picking_id = self.env['stock.picking'].search(['&',
				("origin","=", self.procurment_id.name),('picking_type_id','=',picking_type_id.id),('state','!=','done'),('state','!=','cancel')
				],limit=1)

			location_id = self.env['stock.location'].search([
																 ('id','=',self.location_id.lot_stock_id.id),
																 ('usage','=','internal')],limit=1)

			location_dest_id = self.env['stock.location'].search([('id','=',self.location_dest_id.lot_stock_id.id),
																	  ('usage','=','internal')],limit=1)

			if picking_id:
				vals.update({"move_ids_without_package":[(0,0, {
													'location_id':location_id.id,
													'location_dest_id':location_dest_id.id,
													'product_id':self.product_id.id,
													'product_uom_qty': qty,
													'product_uom':self.uom_id.id,
													'name':self.product_id.name
													   
				})]

					})
				res= picking_id.sudo().write(vals)
			else:
				if picking_type_id:
				
					vals = {
						#"applicant":self.procurment_id.demandeur_id.id,
						"scheduled_date":self.procurment_id.date,
						'origin':self.procurment_id.name,
						'priority':'1',
						'invoice_state':'none',
						'state':'waiting',
						'note':self.procurment_id.note,
						'picking_type_id':picking_type_id.id,
						'move_type':'direct',
						'location_id':location_id.id,
						'type_pick':'bsm',
						'location_dest_id':location_dest_id.id,
						"move_ids_without_package":[(0,0, {

														'product_id':self.product_id.id,
														'product_uom_qty': qty,
														'product_uom':self.uom_id.id,
														'name':self.product_id.name,
														   
					})]
					}
				res=self.env['stock.picking'].create(vals)
				self.procurment_id.picking = True
		return res

	@api.one
	def create_tranfert_picking(self):
		res = self.create_picking('Transferts',float(self.uom_qty))
		self.operation = 'transferred' if res else ''
		return res

	@api.one
	def create_bsm_picking(self):
		product_id = self.product_id.id
		date = self.date
		uom_qty = self.uom_qty
		project_id = self.env['project.project'].search([('id','=',self.procurment_id.project_id.id)],limit=1)
		check_qty_budget = self.env['budget.line'].search([('product_id','=',product_id),('date_from','<=',date),('date_to','>=',date),('project_id','=',project_id.id)],limit=1)
		if check_qty_budget and check_qty_budget.budget_id.state == 'done' and (float(uom_qty) + float(check_qty_budget.real_qty_out)) > float(check_qty_budget.qty):
			raise UserError("Attention , Vous avez dépassé la quantité déclarée dans le budget"+'>'+str(check_qty_budget.qty))

		res =  self.create_picking('Consommation',float(uom_qty))
		self.operation = 'consum' if res else ''
		return res

				

# class ValidationAppro(models.TransientModel):
# 	_name = 'validation.approvisionnement'

# 	# approvisionnement_id = fields.Many2one('da.procurment')
# 	# type_validation = fields.Selection([('demande','Confirmation du demandeur'),('magasin','Validation magasin'),('achat','Validation acheteur')])
# 	# signature = fields.Binary('Signature',help='Signature du magasinier',copy=False,attachment=True,max_with=1024,max_height=1024)


# 	# @api.model
# 	# def search_employee(self,user_id):
# 	# 	for record in self:
# 	# 		employee = self.env['hr.employee'].search([('user_id', '=', user_id)],limit=1)
# 	# 		if employee:
# 	# 			return employee.id
# 	# 		else:
# 	# 			raise ValidationError("Prière d'indiquer l'utilisateur de l'employé %s", user_id.name)

# 	# @api.one
# 	# def confirme_appro(self):
# 	# 	res = self.approvisionnement_id.write({'validator_id':self.search_employee(self.env.user.id),
# 	# 										   'signature_magasin':self.signature,
# 	# 										   'state':'done'
# 	# 									})
# 	# 	return res


# 	# @api.one
# 	# def confirme_demandeur(self):
# 	# 	res = self.approvisionnement_id.write({
# 	# 									 'signature_demandeur':self.signature,
# 	# 									 'state':'sent'
# 	# 									})
# 	# 	return res
		
