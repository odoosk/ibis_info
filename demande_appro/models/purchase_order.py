# -*- coding: utf-8 -*-
###################################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: cybrosys(<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################################################
import pytz
import sys
import datetime
import logging
import binascii
from struct import unpack
from odoo import api, fields, models
from odoo import _
from odoo.exceptions import UserError, ValidationError
_logger = logging.getLogger(__name__)

def tradd(num):
	global t1,t2
	ch=''
	if num==0 :
		ch=''
	elif num<20:
		ch=t1[num]
	elif num>=20:
		if (num>=70 and num<=79)or(num>=90):
			z=int(num/10)-1
		else:
			z=int(num/10)
		ch=t2[z]
		num=num-z*10
		if (num==1 or num==11) and z<8:
			ch=ch+' et'
		if num>0:
			ch=ch+' '+tradd(num)
		else:
			ch=ch+tradd(num)
	return ch


def tradn(num):
	global t1,t2
	ch=''
	flagcent=False
	if num>=1000000000:
		z=int(num/1000000000)
		ch=ch+tradn(z)+' milliard'
		if z>1:
			ch=ch+'s'
		num=num-z*1000000000
	if num>=1000000:
		z=int(num/1000000)
		ch=ch+tradn(z)+' million'
		if z>1:
			ch=ch+'s'
		num=num-z*1000000
	if num>=1000:
		if num>=100000:
			z=int(num/100000)
			if z>1:
				ch=ch+' '+tradd(z)
			ch=ch+' cent'
			flagcent=True
			num=num-z*100000
			if int(num/1000)==0 and z>1:
				ch=ch+'s'
		if num>=1000:
			z=int(num/1000)
			if (z==1 and flagcent) or z>1:
				ch=ch+' '+tradd(z)
			num=num-z*1000
		ch=ch+' mille'
	if num>=100:
		z=int(num/100)
		if z>1:
			ch=ch+' '+tradd(z)
		ch=ch+" cent"
		num=num-z*100
		if num==0 and z>1:
		   ch=ch+'s'
	if num>0:
		ch=ch+" "+tradd(num)
	return ch


def trad(nb, unite="Dinar", decim="centime"):
	global t1,t2
	nb=round(nb,2)
	t1=["","un","deux","trois","quatre","cinq","six","sept","huit","neuf","dix","onze","douze","treize","quatorze","quinze","seize","dix-sept","dix-huit","dix-neuf"]
	t2=["","dix","vingt","trente","quarante","cinquante","soixante","soixante-dix","quatre-vingt","quatre-vingt dix"]
	z1=int(nb)
	z3=(nb-z1)*100
	z2=int(round(z3,0))
	if z1==0:
		ch="zéro"
	else:
		ch=tradn(abs(z1))
	if z1>1 or z1<-1:
		if unite!='':
			ch=ch+" "+unite+'s'
	else:
		ch=ch+" "+unite
	if z2>0:
		ch=ch+tradn(z2)
		if z2>1 or z2<-1:
			if decim!='':
				ch=ch+" "+decim+'s'
		else:
			ch=ch+" "+decim
	if nb<0:
		ch="moins "+ch
	return ch


class PurchaseOrder(models.Model):
	_inherit = 'purchase.order'


	request_ids=fields.Many2many("purchase.request",string="Demandes d'achats")
	amount_text = fields.Char(compute='get_amount_text')
	project_id = fields.Many2one('project.project','Projet')
	# validation = fields.Selection([('niv0','A valider'),('niv1','Validation CDA'),('niv2','Validation DFC'),('niv3','Validation DG')],default='niv0' , copy=False)
	# date_validate_1 = fields.Datetime(string='Date validation CDA',copy=False)
	# date_validate_2 = fields.Datetime(string='Date validation DFC',copy=False)
	# date_validate_3 = fields.Datetime(string='Date validation DG',copy=False)
	# user_cda = fields.Many2one('res.users','CDA')
	# user_dfc = fields.Many2one('res.users','DFC')

	@api.depends('amount_total')
	def get_amount_text(self):
		amount = self.amount_text = trad(self.amount_total,'Dinar')
		return amount

	def button_approve(self,force=False):
		self.name = self.name.replace('DP','BC')
		res = super(PurchaseOrder, self).button_approve(force=False)
		procurment_order = self.env['purchase.request'].search([('id','in',self.request_ids.ids)])
		for order in procurment_order:
			order.write({'state':'done'})
		return res

	@api.model
	def _prepare_picking(self):
		res = super(PurchaseOrder, self)._prepare_picking()
		if not res.get('type_pick'):
			res['type_pick'] = 'br'
		return res

	@api.multi
	def button_confirm(self):
		res = super(PurchaseOrder, self).button_confirm()
		self.name = self.name.replace('DP','BC')
		return res



	# @api.one
	# def do_approuve_niv2(self):
	# 	if self.state in ('to approve','purchase','done') and self.validation == 'niv1':
	# 		self.validation = 'niv2'
	# 		self.date_validate_2 = datetime.datetime.today()
	# 		self.user_dfc = self.env.user.id

	# @api.one
	# def do_approuve_niv3(self):
	# 	if self.validation == 'niv2' and self.state in ('to approve','purchase','done'):
	# 		self.validation = 'niv3'
	# 		self.date_validate_3 = datetime.datetime.today()
	# 		self.user_id = self.env.user.id

	@api.multi
	def print_report(self):
			return self.env.ref('demande_appro.action_report_commande').report_action(self)


	@api.multi
	def open_demande_achat(self):
		self.ensure_one()
		#request = self.env['purchase.request'].search([('id', 'in', self.request_ids.ids)])
		action = self.env['ir.actions.act_window'].for_xml_id('demande_appro',"purchase_request_action")
		action["context"] = {"create": False}
		if len(self.request_ids.ids) > 1:
			action['domain'] = [('id', 'in', self.request_ids.ids)]
		elif len(self.request_ids.ids) == 1:
			form_view = [(self.env.ref('demande_appro.purchase_request_view_form').id, 'form')]
			if 'views' in action:
				action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
			else:
				action['views'] = form_view
			action['res_id'] = self.request_ids.ids[0]
		else:
			action = {'type': 'ir.actions.act_window_close'}
		return action