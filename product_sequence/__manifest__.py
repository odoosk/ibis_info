# Copyright 2004 Tiny SPRL
# Copyright 2016 Sodexis
# Copyright 2018 Eficent Business and IT Consulting Services S.L.
#   (http://www.eficent.com)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Product Sequence',
    'version': '12.0.1.0.1',
    'author': "Zikzakmedia SL,Sodexis,Odoo Community Association (OCA)",
    'website': 'https://github.com/OCA/product-attribute',
    'license': 'AGPL-3',
    'category': 'Product',
    'depends': [
        'product',
        'product_code_unique',
        'purchase',
        'stock',
    ],
    'data': [
        'data/product_sequence.xml',
        'data/product_data.xml',
        'data/uom_data.xml',
        'views/product_category.xml',
        'views/sequence_views.xml',
        'views/product_views.xml',
    ],
    'pre_init_hook': 'pre_init_hook',
    'installable': True,
}
