# Copyright (C) 2017 Creu Blanca
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

from odoo import fields, models, api
from datetime import timedelta, date as datetime_date
from dateutil.relativedelta import relativedelta


class IrSequence(models.Model):
    _inherit = "ir.sequence"

    @api.multi
    def reset_sequence_number_next_actual(self):
        sequences = self.env['ir.sequence'].sudo().search([])
        for sequence in sequences:
            if not sequence.use_date_range:
                sequence.number_next_actual = 1