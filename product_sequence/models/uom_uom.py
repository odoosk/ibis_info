# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, tools, models, _
from odoo.exceptions import UserError, ValidationError


class UoMCategory(models.Model):
    _inherit = 'uom.category'

    measure_type = fields.Selection(selection_add=[('linear', 'Linear length'),
                                                   ('unit_lot', 'Lot'),
                                                   ('unit_fdx', 'Fdx'),
                                                   ('unit_bte', 'Btl'),
                                                   ('unit_crt', 'Crt'),
                                                   ('unit_fut', 'Fûs'),
                                                   ('unit_rlx', 'Rlx'),
                                                   ('unit_btl', 'Btl'),
                                                   ('area', 'Area'),
                                                   ('unit_can', 'Bidon'),
                                                   ('unit_kit', 'Kit'),
                                                   ('unit_pack', 'Pack'),
                                                   ('unit_package', 'Paquet'),
                                                   ('unit_sheet', 'PLAQUE'),
                                                   ('unit_ram', 'Rame'),
                                                   ('unit_bag', 'Sac'),
                                                   ('unit_tube', 'Tube'),
                                                   ('unit_board', 'PLANCHE'),
                                                   ('unit_bottle', 'FLACON'),
                                                   ('unit_notebook', 'CARNET'),
                                                   ('unit_card', 'CARTE'),
                                                   ('unit_year', 'ANNÉE'),
                                                   ('unit_month', 'MOIS'),
                                                   ('unit_week', 'SEMAINE'),
                                                   ('unit_days', 'JOURS'),
                                                   ('unit_travel', 'VOYAGE'),
                                                   ('unit_rotation', 'ROTATION'),
                                                   ('unit_booking', 'RESERVATION'),
                                                   ('unit_person', 'PERSONNE'),
                                                   ('unit_panneau', 'PANNEAU'),
                                                   ])
