# Copyright 2018 Eficent Business and IT Consulting Services S.L.
#   (http://www.eficent.com)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models


class ProductCategory(models.Model):
    _inherit = 'product.category'

    code_prefix = fields.Char(
        string="Prefix for Product Internal Reference",
        help="Prefix used to generate the internal reference for products "
             "created with this category. If blank the "
             "default sequence will be used.",
    )
    sequence_id = fields.Many2one(
        comodel_name="ir.sequence", string="Product Sequence",
        help="This field contains the information related to the numbering "
             "of the journal entries of this journal.",
        copy=False, readonly=True,
    )
    is_sub_category = fields.Boolean("Is Subcategory")

    @api.model
    def _prepare_ir_sequence(self, prefix):
        """Prepare the vals for creating the sequence
        :param prefix: a string with the prefix of the sequence.
        :return: a dict with the values.
        """
        vals = {
            "name": "Product " + prefix,
            "code": "product.product - " + prefix,
            "padding": 3,
            "prefix": prefix,
            "company_id": False,
        }
        return vals

    @api.multi
    def write(self, vals):
        prefix = vals.get("code_prefix")
        if prefix:
            if self.parent_id and self.parent_id.code_prefix:
                prefix = self.parent_id.code_prefix + prefix
            if self.parent_id.parent_id and self.parent_id.parent_id.code_prefix:
                prefix = self.parent_id.parent_id.code_prefix + prefix
            for rec in self:
                if rec.sequence_id:
                    rec.sudo().sequence_id.name = "Product " + prefix
                    rec.sudo().sequence_id.code = "product.product - " + prefix
                    rec.sudo().sequence_id.prefix = prefix
                else:
                    seq_vals = self._prepare_ir_sequence(prefix)
                    rec.sequence_id = self.env["ir.sequence"].create(seq_vals)
        return super().write(vals)

    @api.model
    def create(self, vals):
        prefix = vals.get("code_prefix")
        if prefix:
            if vals.get("parent_id"):
                parent = self.env['product.category'].browse(vals.get("parent_id"))
                if parent.code_prefix:
                    prefix = parent.code_prefix + prefix
                if parent.parent_id and parent.parent_id.code_prefix:
                    prefix = parent.parent_id.code_prefix + prefix
            seq_vals = self._prepare_ir_sequence(prefix)
            sequence = self.env["ir.sequence"].create(seq_vals)
            vals["sequence_id"] = sequence.id
        return super().create(vals)


