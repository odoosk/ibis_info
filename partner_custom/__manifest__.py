# -*- coding: utf-8 -*-
##########################################################################
###################################################################################
{
    'name': 'FISCAL PARTNER INFORMATION',
    'version': '13.0.1.1.2',
    'summary': """coordonnés fiscaux des partenaires""",
    'description': """  """,
    'category': 'accounting',
    'author': 'SEKKAK OUSSAMA / Elosys',
    'company': 'OSK FREELANCE',
    'website': "",
    'depends': ['base','sale'],
    'data': [
        'views/res_partner.xml',
        'views/res_company.xml',
        'reports/account_invoice_report.xml',
        'reports/inherit_header_footer.xml',
        'reports/sale_invoice_report.xml'

    ],
    'icon_image': ['static/description/icon.png'],
    'images': ['static/description/access_group.png','static/description/session.png','static/description/session_trainer.png'],
    'license': 'AGPL-3',
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
